data class Point constructor(var x: Double, var y: Double) {
    fun addPoint(p: Point): Point {
        this.x = this.x + p.x
        this.y = this.y + p.y
        return this
    }

    fun symetrical() {
        this.x = -this.x
        this.y = -this.y
    }
}

